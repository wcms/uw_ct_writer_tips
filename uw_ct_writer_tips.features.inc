<?php

/**
 * @file
 * uw_ct_writer_tips.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function uw_ct_writer_tips_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "feeds" && $api == "feeds_importer_default") {
    return array("version" => "1");
  }
  if ($module == "services" && $api == "services") {
    return array("version" => "3");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function uw_ct_writer_tips_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_node_info().
 */
function uw_ct_writer_tips_node_info() {
  $items = array(
    'uw_writer_tip' => array(
      'name' => t('Writer Tip'),
      'base' => 'node_content',
      'description' => t('Random tip that will appear when adding or editing content'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => t('Enter an admin title for this writer tip - used for the content admin pages so that you can identify the tip in the listing'),
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
