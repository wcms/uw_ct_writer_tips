<?php

/**
 * @file
 * uw_ct_writer_tips.services.inc
 */

/**
 * Implements hook_default_services_endpoint().
 */
function uw_ct_writer_tips_default_services_endpoint() {
  $export = array();

  $endpoint = new stdClass();
  $endpoint->disabled = FALSE; /* Edit this to true to make a default endpoint disabled initially */
  $endpoint->api_version = 3;
  $endpoint->name = 'services_writer_tip';
  $endpoint->server = 'rest_server';
  $endpoint->path = 'api/v1/writer-tip';
  $endpoint->authentication = array();
  $endpoint->server_settings = array();
  $endpoint->resources = array(
    'list' => array(
      'operations' => array(
        'index' => array(
          'enabled' => '1',
        ),
      ),
    ),
    'views' => array(
      'operations' => array(
        'retrieve' => array(
          'enabled' => '1',
        ),
      ),
    ),
  );
  $endpoint->debug = 0;
  $export['services_writer_tip'] = $endpoint;

  return $export;
}
